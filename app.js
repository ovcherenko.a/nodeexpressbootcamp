const path = require('path')
const express = require('express');
const morgan = require('morgan');
const rateLimit = require('express-rate-limit');
const helmet = require('helmet');
const mongoSanitize = require('express-mongo-sanitize');

// Data sanitization, preventing parameters polution
const xss = require('xss-clean');
const hpp = require('hpp')

// Compression
const compression = require('compression')

//Importing routes
const tourRouter = require(`./routes/tourRoutes`);
const userRouter = require(`./routes/userRoutes`);
const reviewRouter = require(`./routes/reviewRoutes`);

//Error class
const AppError = require('./utils/appError')
const globalErrorHandler = require('./controllers/errorController');

//Server
const app = express();


//ADDING ACCESS TO PUBLIC FOLDER
// app.use(express.static( `${__dirname}/public` ))
app.use(express.static(path.join(__dirname, 'public')))
app.set('view engine', 'pug')
app.set('views', path.join(__dirname, 'views'))


// MIDDLEWARES GLOBAL

//SET SECURITY HTTP
app.use(helmet())

//DEVELOPMENT LOGGING
if(process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'))
}

//LIMIT REQUEST FROM SAME IP
const limiter = rateLimit({
    max: 100,
    windowMs: 60 * 60 * 1000,
    message: 'Too many request from this IP. Please try again in hour'
});

app.use('/api', limiter);


//BODY PARSES. reading data from body into req.body
app.use(express.json({
    limit: '50kb'
}))


//DATA sanitization against NoSQL query injection
app.use(mongoSanitize())


//Data sanitization against XSS
app.use(xss());

// Prevent parameter polution
app.use(hpp({
    whitelist: [
        'duration', 
        'maxGroupSize', 
        'ratingAverage', 
        'ratingsQuantity', 
        'maxGroupSize', 
        'difficulty', 
        'price'
    ]
}))

app.use(compression())



// Пример middleware
// app.use( (req, res, next) => {
//     console.log('Hello from middleware 👋')
//     next()
// });

//Добавление переменной в запрос с помощью middleware
app.use( (req, res, next) => {
 req.requestTime = new Date().toISOString();

    next()
})


app.get('/', (req, res) => {
    res.status(200).render('base')
})
app.get('/test', (req, res) => {
    res.status(200).render('testPage')
})

app.use('/test', (req, res) => {
    res.status(200).render('testPage')
})

app.use('/api/v1/tours', tourRouter)
app.use('/api/v1/users', userRouter)
app.use('/api/v1/reviews', reviewRouter)

app.all('*', (req, res, next) => {
    // res.status(404).json({
    //     status: 'fail',
    //     message: `Cant find ${req.originalUrl} on this server!`
    // })

    // const err = new Error(`Can't find ${req.originalUrl} on this server`);
    // err.status = 'fail';
    // err.statusCode = 404;

    next(new AppError(`Can't find ${req.originalUrl} on this server`, '404'));
})

app.use(globalErrorHandler)
// app.get('/api/v1/tours', getAllTours);
// app.get('/api/v1/tours/:id/', getTour);
// app.post('/api/v1/tours', createTour)
// app.patch('/api/v1/tours/:id', updateTour)
// app.delete('/api/v1/tours/:id', deleteTour)
 
module.exports = app;