const express = require('express');
const userController = require('./../controllers/userController');
const authController = require('./../controllers/authController');

const { route } = require('./tourRoutes');

const router = express.Router();

router
    .post('/signup', authController.signup)
    .post('/login', authController.login)

    .post('/forgotPassword', authController.forgotPassword)
    .patch('/resetPassword/:token', authController.resetPassword)
    .patch('/updateMyPassword/', authController.protect, authController.updatePassword)
    .patch('/updateMe', authController.protect, userController.updateMe)
    .delete('/deleteMe', authController.protect, userController.deleteMe)

router
    .route('/')
    .get(userController.getAllUsers)
    .post(userController.createUser);

router
    .route('/:id')
    .get(userController.getOneUser)
    .patch(userController.updateUser)
    .delete(userController.deleteUser);

module.exports = router