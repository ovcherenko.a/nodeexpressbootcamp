const crypto = require('crypto');
const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Account must have a name'],
        unique: true,
    },
    email: {
        type: String,
        required: [true, 'Account must have email'],
        unique: true,
        lowercase: true,
        validate: [validator.isEmail, 'Please provide a valid email']
    },
    photo: String,
    role: {
        type: String,
        enum: ['user', 'guide', 'lead-guide', 'admin'],
        default: 'user' 
    },
    password: {
        type: String,
        required: [true, 'Account must have a password'],
        minlength: 8,
        select: false
    },
    passwordConfirm: {
        type: String,
        required: [true, 'You must reenter your password here'],
        validate: {
            //THIS WORKS ONLY ON SAVE
            validator: function(val) {
                return val === this.password;
            },
            message: 'Confirmation password and password must be the same'
        }
    },
    passwordChangedAt: {
        type: Date
    },
    passwordResetToken: String,
    passwordResetExpires: Date,
    active: {
        type: Boolean,
        default: true,
        select: false
    }
})

userSchema.pre('save', async function(next) {
    if(!this.isModified('password')) return next()

    console.log(this)

    // hash the password with cost of 12
    this.password = await bcrypt.hash(this.password, 12)

    this.passwordConfirm = undefined;
    next()
})

userSchema.pre('save', function(next) {
    if (!this.isModified('password') || this.isNew) return next()

    this.passwordChangedAt = Date.now() - 1000;
    next();
})

userSchema.pre(/^find/, function(next){
    //this points to current query

    this.find({active: { $ne: false }})
    next()
})

userSchema.methods.correctPassword = async function(candidatePassword, userPassword) {
    return await bcrypt.compare(candidatePassword, userPassword)
}

userSchema.methods.changePasswordAfter = async function(JWTTimestamp) {
    // console.log('passwordChangedAt')
    // console.log(this.passwordChangedAt)
    if(this.passwordChangedAt) {
        const changedTimestamp = parseInt(this.passwordChangedAt.getTime() / 1000, 10)
        // console.log(JWTTimestamp)
        // console.log(changedTimestamp)
        // console.log(JWTTimestamp < changedTimestamp)
        return JWTTimestamp < changedTimestamp;
    }

    return false;
}

userSchema.methods.createPasswordResetToken = function() {
    const resetToken = crypto.randomBytes(32).toString('hex');

    this.passwordResetToken = crypto
        .createHash('sha256')
        .update(resetToken)
        .digest('hex');

    this.passwordResetExpires = Date.now() + 10 * 60 * 1000;

    return resetToken;
}

const User = mongoose.model('User', userSchema)

module.exports = User;