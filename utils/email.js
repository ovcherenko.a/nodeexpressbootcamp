const nodemailer = require('nodemailer');

const sendEmail = async options => {
    // 1 Create a transporter

    var transporter = nodemailer.createTransport({
        // service: "Gmail",
        // Activate in gmail 'less secure app" option
        // With GMAIL you no need host and port

        host: process.env.EMAIL_HOST,
        port: process.env.EMAIL_PORT,
        auth: {
            user: process.env.EMAIL_USERNAME,
            pass: process.env.EMAIL_PASSWORD
        }
    })

    // 2 Define email options
    const mailOptions = {
        from: 'Anton <Ololo@gmail.com>',
        to: options.email,
        subject: options.subject,
        text: options.message,
        // html: 
    }

    // 3 Actually send email
    const answer = await transporter.sendMail(mailOptions)
    console.log(answer)
    return answer;
}

module.exports = sendEmail;