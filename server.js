// const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')
dotenv.config({path: './config.env'})
const app = require('./app')

process.on('uncaughtException', err => {
    console.log('unhandled exception. shutting down')
    console.log(err.name, err.message)
    process.exit(1)
})

//Настройка mongoose
// const DB1 = process.env.DATABASE.replace('<PASSWORD>', process.env.DATABASE_PASSWORD)

const DB = 'mongodb+srv://durchik:G67cfxi9rHDH9I91@cluster0-nzotr.mongodb.net/natours?retryWrites=true&w=majority'

mongoose.connect(DB, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false
}).then(() => {console.log('DB connected successful')})

mongoose.set('useNewUrlParser', true)

//START SERVER
const port = process.env.PORT || 3000;
const server = app.listen(port, () => {
    console.log(`app running on port ${port}`)
})

process.on('unhandledRejection', err => {
    console.log('unhandled rejection. shutting down')
    console.log(err.name, err.message)
    server.close( () => {
        process.exit(1)
    })
})
