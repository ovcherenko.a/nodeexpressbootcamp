const fs = require('fs');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const Tour = require('./../../models/tourModel')

//Хардкод доступа к базе данных
const DATABASE='mongodb+srv://durchik:<PASSWORD>@cluster0-nzotr.mongodb.net/natours?retryWrites=true&w=majority';
let DATABASEPASSWORD ='G67cfxi9rHDH9I91';


dotenv.config({ path: './config.env' })

const DB = DATABASE.replace(
    '<PASSWORD>',
    DATABASEPASSWORD
)

// const DB = process.env.DATABASE.replace(
//     '<PASSWORD>',
//     process.env.DATABASE_PASSWORD
// )


mongoose.connect(DB, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false
}).then(() => {
    console.log('DB connection succesful')
})

const tours = JSON.parse(fs.readFileSync( `${__dirname}/tours-simple.json`, 'utf-8'))

//IMPORT DATA INTO DB
const importData = async () => {
    try {
        await Tour.create(tours)
        console.log('Data succesfully loaded')
    } catch (err) {
        console.log(err)
    }
    process.exit()
}



// DELETE ALL DATA FROM DB

const deleteData = async () => {
    try{
        await Tour.deleteMany()
        console.log('Data deleted from DB')
    } catch (err) {
        console.log(err)
    }
    process.exit()
}

if(process.argv[2] === "--import") {
    importData()
} else if (process.argv[2] === '--delete') {
    deleteData()
}

console.log(process.argv)