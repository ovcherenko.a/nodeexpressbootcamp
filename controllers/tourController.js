const Tour = require('./../models/tourModel')
const APIFeatures = require('./../utils/apiFeatures')
const catchAsync = require('./../utils/catchAsync')
const AppError = require('./../utils/appError')

//UPLOADING DATABASE FROM FILE JUST FOR PRACTICE
// const tours = JSON.parse(fs.readFileSync(`${__dirname}/../dev-data/data/tours-simple.json`))

// exports.checkId = (req, res, next, value) => {
//     // console.log('checking id')
//     let specificTour = tours.find(element => element.id === +value) 
//     if(!specificTour) {
//         return res.status(404).json({
//             status: "Fail",
//             message: "Invalid ID"
//         })
//     }
//     next();
// }
 
// exports.checkBody = (req, res, next) => {
//     if(!req.body.name ||!req.body.price ) {
//         return res.status(400).json({
//             status: "Fail",
//             message : "Missing name or price"
//         })
//     }
//     next()
// }

exports.aliasTopTours = async (req, res, next) => {
    req.query.limit = '5';
    req.query.sort = '-ratingAverage, price'
    req.query.fields = 'name,price,retingsAverage,summary,difficulty'
    next();
}

exports.getAllTours = catchAsync(async (req, res, next) => {
    //EXECUTE QUERY
    const features = new APIFeatures(Tour.find(), req.query)
        .filter()
        .sort()
        .limitFields()
        .paginate()

    const tours = await features.query;

    res.status(200).json({
        status: 'success',
        result: tours.length,
        requestedAt: req.requestTime,
        data: {
            tours: tours
        }
    })
})

exports.getTour = catchAsync(async (req,res, next) => {
    // console.log(req.params)
    const specificTour = await (await Tour.findById(req.params.id)).populate('reviews')
    
    if(!specificTour) {
        return next(new AppError('No tour found with that id', 404))
    }
    
    res.status(200).json({
        status: "success",
        data: {
            tours: specificTour
        }
    })

})

exports.createTour = catchAsync(async (req, res, next) => {
    const newTour = await Tour.create(req.body)
    res.status(201).json({
        status: 'success',
        data: {
            tour: newTour
        }
    })


    // try {

    // } catch (err) {
    //     res.status(400).json({
    //         status: "fail",
    //         message: "Invalid data sent",
    //         errorMessage: err
    //     })
    // }

    // const newId = tours.length+1;
    // const newTour = Object.assign({"id": newId}, req.body)
    // tours.push(newTour);
    // fs.writeFile(`${__dirname}/dev-data/data/tours-simple.json`, JSON.stringify(tours), err => {
        // res.status(201).json({
        //     status: 'success',
        //     data: {
        //         tour: newTour
        //         }
        //     }
        // )
    // })
})

exports.updateTour = catchAsync(async (req, res, next) => {
    // const requestedElementId = +req.params.id;
    let updatedTour = await Tour.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
        runValidators: true
    })

    if(!updatedTour) {
        return next(new AppError('No tour found with that id', '404'))
    }

    res.status(204).json({
        status: "success",
        data: {
            tour: updatedTour
        }
    })
})

exports.deleteTour = catchAsync(async (req, res, next) => {
    const deletedTour = await Tour.findByIdAndDelete(req.params.id)
    
    if(!deletedTour) {
        return next(new AppError('No tour found with that id', 404))
    }
    
    res.status(204).json({
        status: "success",
        message: "Tour deleted"
    })

    // tours.forEach( (element, index) => {
    //     if(element.id === requestedElementId) {
    //         tours.splice(index, 1)
    //     }
    // })
    // fs.writeFile(`${__dirname}/dev-data/data/tours-simple.json`, JSON.stringify(tours), err=>{
    //     if(err) {console.log(err)}
    // })
})

exports.getTourStats = catchAsync(async (req, res, next) => {
    const stats = await Tour.aggregate([
        {
            $match: { ratingAverage: {$gte: 4.5} }
        },
        {
            $group: {
                // _id: '$price', 
                _id: { $toUpper: '$difficulty'}, 
                numTours: { $sum: 1 },
                numRatings: { $sum: '$ratingsQuantity'},
                avgRating: { $avg: '$ratingAverage' },
                avgPrice: { $avg: '$price' },
                minPrice: { $min: '$price' },
                maxPrice: { $max: '$price' }
            }
        },
        {
            $sort: { avgPrice: 1 }
        },
        {
            $match: { _id: { $ne: 'MEDIUM'} }
        }
    ])
    console.log(stats)
    res.status(200).json({
        status: 'success',
        data: {
            stats
        }
    })
})

exports.getMounthlyPlan = catchAsync(async (req, res, next) => {
    const year = req.params.year * 1;
    console.log(year)

    const plan = await Tour.aggregate([
        {
            $unwind: '$startDates'
        },
        {
            $match: {
                startDates: {
                    $gte: new Date(`${year}-01-01`),
                    $lte: new Date(`${year}-12-31`)
                }
            }
        },
        {
            $group: {
                _id: { $month: '$startDates'},
                numTourStarts: { $sum: 1 },
                tours: { $push: '$name'}
            }
        },
        {
            $addFields: { month: '$_id'}
        },
        {
            $project: {
                _id: 0
            }
        },
        {
            $sort: {numTourStarts: -1 }
        },
        {
            $limit: 12
        }
    ])

    res.status(200).json({
        status: 'success',
        message: {
            plan: plan
        }            
    })
})